package com.itau.dojoCI;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
	public void testeBuzzValida5() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(5);
		assertEquals("Buzz", retorno);	
	}
	
	public void testBuzzValida3() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(3);
		assertEquals("Fizz", retorno);	
	}
	
	
	public void testBuzzValida3e5() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(15);
		assertEquals("FizzBuzz", retorno);	
	}

	
	public void testBuzzValidaDiferente() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(2);
		assertEquals("2", retorno);	
	}

	
	public void testBuzzValida6() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(6);
		assertEquals("Fizz", retorno);	
	}

	
	public void testBuzzValida10() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(10);
		assertEquals("Buzz", retorno);	
	}
		

	public void testBuzzValida30() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(30);
		assertEquals("FizzBuzz", retorno);	
	}
	
	public void testBuzzValidaDiferente4() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.validaNumero(4);
		assertEquals("4", retorno);	
	}
}
